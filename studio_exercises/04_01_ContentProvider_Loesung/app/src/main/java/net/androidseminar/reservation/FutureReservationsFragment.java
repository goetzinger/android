package net.androidseminar.reservation;

import java.util.Calendar;

import android.app.ListFragment;
import android.content.ContentResolver;
import android.database.Cursor;
import android.os.Bundle;

public class FutureReservationsFragment extends ListFragment {

	private ReservationListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ContentResolver contentResolver = getActivity().getContentResolver();
		long nowInMillis = Calendar.getInstance().getTime().getTime();
		Cursor cursor = contentResolver.query(
				ReservationContentProvider.CONTENT_URI, null, "von > ?",
				new String[] { nowInMillis + "" }, null);
		getActivity().startManagingCursor(cursor);
		adapter = new ReservationListAdapter(getActivity(), cursor);
		setListAdapter(adapter);
	}

}

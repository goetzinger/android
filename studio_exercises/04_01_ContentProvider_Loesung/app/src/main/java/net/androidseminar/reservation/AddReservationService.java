package net.androidseminar.reservation;

import java.io.Serializable;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class AddReservationService extends Service {

	private Reservation toAdd;

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int returnFromSuper = super.onStartCommand(intent, flags, startId);
		Bundle bundle = intent.getExtras();
		Serializable serializable = bundle.getSerializable("reservation");
		if (serializable instanceof Reservation) {
			toAdd = (Reservation) serializable;
			Log.i(this.getClass().getName(), toAdd.toString());
			ContentValues values = new ContentValues();
			values.put("fahrzeug", toAdd.getFahrzeug());
			values.put("filiale", toAdd.getFiliale());
			values.put("von", toAdd.getVon().getTime());
			values.put("bis", toAdd.getBis().getTime());
			System.out.println(toAdd.getVon());
			getContentResolver().insert(ReservationContentProvider.CONTENT_URI,
					values);
		}

		return returnFromSuper;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
	}
}

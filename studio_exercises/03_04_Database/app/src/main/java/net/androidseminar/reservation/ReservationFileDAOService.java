package net.androidseminar.reservation;

import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class ReservationFileDAOService extends Service {

	private final List<Reservation> reservationsCache = new LinkedList<Reservation>();
	private static final String filename = "reservations.ser";

	@Override
	public void onCreate() {
		super.onCreate();
		readReservationsFromFile();
	}

	private void readReservationsFromFile() {
		String[] privateFiles = fileList();
		int indexOfFile = Arrays.binarySearch(privateFiles, filename);
		FileInputStream fos;
		ObjectInputStream ois = null;
		if (indexOfFile == -1) {
			File f = new File(getFilesDir(), filename);
			try {
				f.createNewFile();
			} catch (IOException e) {
				Log.e(this.getClass().getName(), "", e);
			}
		}
		try {
			fos = openFileInput(filename);

			ois = new ObjectInputStream(fos);

			while (fos.available() > 0) {
				Reservation reservation = (Reservation) ois.readObject();
				if (reservation != null)
					this.reservationsCache.add(reservation);
			}
		} catch (EOFException e) {
			Log.d(this.getClass().getName(), "", e);
		} catch (Exception e) {
			Log.e(this.getClass().getName(), "", e);
		} finally {
			try {
				if (ois != null)
					ois.close();
			} catch (IOException e) {
			}
		}

	}

	@Override
	public IBinder onBind(Intent intent) {
		return new LocalBinder();
	}

	public List<Reservation> getAllReservations() {
		ArrayList<Reservation> copy = new ArrayList<Reservation>(
				reservationsCache.size());
		copy.addAll(reservationsCache);
		return copy;
	}

	public void addReservations(Reservation toAdd) {
		this.reservationsCache.add(toAdd);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		writeCache2File();
	}

	private void writeCache2File() {
		ObjectOutputStream oos = null;
		try {
			FileOutputStream fileOutput = openFileOutput(filename, MODE_PRIVATE);
			oos = new ObjectOutputStream(fileOutput);
			for (Reservation res2Write : reservationsCache) {
				oos.writeObject(res2Write);
			}
		} catch (IOException e) {
			Log.e(this.getClass().getName(), "", e);
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
			}
		}
	}

	class LocalBinder extends Binder {

		ReservationFileDAOService getService() {
			return ReservationFileDAOService.this;
		}
	}
}

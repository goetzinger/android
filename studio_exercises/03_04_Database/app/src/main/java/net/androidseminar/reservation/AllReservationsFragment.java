package net.androidseminar.reservation;

import net.androidseminar.intent.loesung.R;
import net.androidseminar.reservation.ReservationFileDAOService.LocalBinder;
import android.app.ListFragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;

public class AllReservationsFragment extends ListFragment {

	private final OnSharedPreferenceChangeListener listener = new PreferencesListener();
	private ReservationListAdapter adapter;
	private ReservationFileDAOService reservationDAO = null;
	ServiceConnection con = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {
			reservationDAO = null;
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			reservationDAO = ((LocalBinder) service).getService();
			adapter.setReservations(reservationDAO.getAllReservations());
		}
	};

	@Override
	public void onStart() {
		super.onStart();
		getActivity().bindService(
				new Intent(getActivity(), ReservationFileDAOService.class),
				con, Context.BIND_AUTO_CREATE);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = getAdapter();
		setListAdapter(adapter);
		PreferenceManager.getDefaultSharedPreferences(
				this.getActivity().getApplicationContext())
				.registerOnSharedPreferenceChangeListener(listener);
	}

	protected ReservationListAdapter getAdapter() {
		return new ReservationListAdapter(this.getActivity());
	}

	@Override
	public void onStop() {
		super.onStop();
		getActivity().unbindService(con);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		PreferenceManager.getDefaultSharedPreferences(
				getActivity().getApplicationContext())
				.unregisterOnSharedPreferenceChangeListener(listener);
	}

	class PreferencesListener implements OnSharedPreferenceChangeListener {

		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {
			String sortationKey = getResources().getString(
					R.string.sortResASCKey);
			if (key.equals(sortationKey))
				adapter.sort(sharedPreferences, sortationKey);
		}
	}

}

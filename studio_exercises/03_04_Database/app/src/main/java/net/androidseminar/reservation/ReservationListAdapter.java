package net.androidseminar.reservation;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import net.androidseminar.intent.loesung.R;
import android.content.Context;
import android.content.SharedPreferences;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

class ReservationListAdapter extends BaseAdapter {

	protected final List<Reservation> reservations = new LinkedList<Reservation>();
	private final Context context;
	private final java.text.DateFormat dateFormat = java.text.DateFormat
			.getDateInstance(java.text.DateFormat.SHORT);

	ReservationListAdapter(Context context) {
		this.context = context;
	}

	void sort(SharedPreferences preferences, String key) {
		boolean sortAscending = preferences.getBoolean(key, true);
		if (sortAscending)
			Collections.sort(reservations);
		else
			Collections.sort(reservations,
					new ReservationDescendingComparator());

		notifyDataSetChanged();
	}

	public int getCount() {
		return reservations.size();
	}

	public Object getItem(int position) {
		return reservations.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Reservation reservation2Display = reservations.get(position);

		View v;
		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(context);
			v = inflater.inflate(R.layout.reservationlistentry, null, false);
		} else
			v = convertView;

		TextView fahrzeugText = ((TextView) v.findViewById(R.id.fahrzeugText));
		fahrzeugText.setText(reservation2Display.getFahrzeug());
		TextView filialeText = ((TextView) v.findViewById(R.id.filialeText));
		filialeText.setText(reservation2Display.getFiliale());
		TextView vonText = ((TextView) v.findViewById(R.id.vonText));
		vonText.setText(dateFormat.format(reservation2Display.getVon()));
		TextView bisText = ((TextView) v.findViewById(R.id.bisText));
		bisText.setText(dateFormat.format(reservation2Display.getBis()));

		return v;
	}

	public void setReservations(List<Reservation> allReservations) {
		this.reservations.clear();
		this.reservations.addAll(allReservations);
		this.notifyDataSetChanged();
	}

}

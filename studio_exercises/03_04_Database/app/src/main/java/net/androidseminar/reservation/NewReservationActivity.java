package net.androidseminar.reservation;

import java.text.DateFormat;
import java.text.ParseException;
import java.util.Date;

import net.androidseminar.intent.loesung.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class NewReservationActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.newreservation);
		Button okButton = (Button) findViewById(R.id.newReservationSubmitButton);
		okButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				if (startService())
					stopActivity();

			}
		});
	}

	protected boolean startService() {
		Intent service = new Intent(this, AddReservationService.class);
		Reservation res = null;
		try {
			res = createReservationByReadingInputFields();
		} catch (ParseException e) {
			Log.e(this.getClass().getName(), "", e);
			return false;
		}
		service.putExtra("reservation", res);
		startService(service);
		return true;
	}

	private Reservation createReservationByReadingInputFields()
			throws ParseException {
		Reservation res = new Reservation();
		EditText carField = (EditText) findViewById(R.id.carTypeeditText);
		res.setFahrzeug(carField.getText().toString());
		EditText locationField = (EditText) findViewById(R.id.locationeditText);
		res.setFiliale(locationField.getText().toString());
		EditText fromDateField = (EditText) findViewById(R.id.fromDateEditText);
		DateFormat formate = DateFormat.getDateInstance(DateFormat.SHORT);
		Date fromDate = formate.parse(fromDateField.getText().toString());
		res.setVon(fromDate);
		EditText toDateField = (EditText) findViewById(R.id.toDateeditText);
		Date toDate = formate.parse(toDateField.getText().toString());
		res.setBis(toDate);
		return res;
	}

	protected void stopActivity() {
		this.finish();
	}

}

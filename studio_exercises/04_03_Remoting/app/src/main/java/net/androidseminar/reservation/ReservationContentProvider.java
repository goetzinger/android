package net.androidseminar.reservation;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;

public class ReservationContentProvider extends ContentProvider {

	public static final String AUTHORITY = ReservationContentProvider.class
			.getName().toLowerCase();
	public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
			+ "/" + ReservationSQLiteOpenHelper.TABLE_NAME);
	public static final int ALL_ENTRIES = 1;
	private static final UriMatcher matcher;

	private ReservationSQLiteOpenHelper databaseHandler;

	static {
		matcher = new UriMatcher(UriMatcher.NO_MATCH);
		matcher.addURI(AUTHORITY, ReservationSQLiteOpenHelper.TABLE_NAME,
				ALL_ENTRIES);
	}

	@Override
	public boolean onCreate() {
		databaseHandler = new ReservationSQLiteOpenHelper(getContext());
		return true;
	}

	@Override
	public Cursor query(Uri uri, String[] projection, String selection,
			String[] selectionArgs, String sortOrder) {
		SQLiteDatabase database = databaseHandler.getReadableDatabase();
		SQLiteQueryBuilder builder = new SQLiteQueryBuilder();
		builder.setTables(ReservationSQLiteOpenHelper.TABLE_NAME);
		if (!(matcher.match(uri) == ALL_ENTRIES))
			throw new IllegalStateException(
					"Currently only select all is supported");
		Cursor cursor = builder.query(database, projection, selection,
				selectionArgs, null, null, null);
		cursor.setNotificationUri(getContext().getContentResolver(), uri);
		return cursor;
	}

	@Override
	public String getType(Uri uri) {
		switch (matcher.match(uri)) {
		case ALL_ENTRIES:
			return "vnd.android.cursor.dir/vnd." + AUTHORITY
					+ ReservationSQLiteOpenHelper.TABLE_NAME;
		default:
			throw new IllegalArgumentException("Unsupported URI: " + uri);
		}
	}

	@Override
	public Uri insert(Uri uri, ContentValues values) {
		SQLiteDatabase database = databaseHandler.getWritableDatabase();
		long row = database.insert(ReservationSQLiteOpenHelper.TABLE_NAME, "",
				values);
		if (row > 0) {
			Uri result = ContentUris.withAppendedId(CONTENT_URI, row);
			getContext().getContentResolver().notifyChange(result, null);
			return result;
		}
		throw new SQLException("Could not insert: " + values + " into " + uri);
	}

	@Override
	public int delete(Uri uri, String selection, String[] selectionArgs) {
		SQLiteDatabase database = databaseHandler.getWritableDatabase();
		int deletedRows = database.delete(
				ReservationSQLiteOpenHelper.TABLE_NAME, selection,
				selectionArgs);
		return deletedRows;
	}

	@Override
	public int update(Uri uri, ContentValues values, String selection,
			String[] selectionArgs) {
		SQLiteDatabase database = databaseHandler.getWritableDatabase();
		int updatedRows = database.update(
				ReservationSQLiteOpenHelper.TABLE_NAME, values, selection,
				selectionArgs);
		return updatedRows;
	}

}

package net.androidseminar.reservation;

import net.androidseminar.remoting.R;
import android.app.ListFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

public class AllReservationsFragment extends ListFragment {

	private final OnSharedPreferenceChangeListener listener = new PreferencesListener();
	private ReservationListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.getLoaderManager().initLoader(0, new Bundle(),
				new ReservationLoaderCallback(this));
		adapter = new ReservationListAdapter(getActivity(), null);

		PreferenceManager.getDefaultSharedPreferences(
				this.getActivity().getApplicationContext())
				.registerOnSharedPreferenceChangeListener(listener);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setListAdapter(adapter);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setListShown(false);
		setEmptyText(getResources().getString(R.string.noContentInList));
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		PreferenceManager.getDefaultSharedPreferences(
				getActivity().getApplicationContext())
				.unregisterOnSharedPreferenceChangeListener(listener);
	}

	class PreferencesListener implements OnSharedPreferenceChangeListener {

		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {
			String sortationKey = getResources().getString(
					R.string.sortResASCKey);
			// if (key.equals(sortationKey))
			// adapter.sort(sharedPreferences, sortationKey);
		}
	}

}

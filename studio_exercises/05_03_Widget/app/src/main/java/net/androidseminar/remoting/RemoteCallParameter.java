package net.androidseminar.remoting;

import java.io.Serializable;
import java.net.URI;

public class RemoteCallParameter implements Serializable {

	public URI uri;
	public RemoteCallback callback;

}

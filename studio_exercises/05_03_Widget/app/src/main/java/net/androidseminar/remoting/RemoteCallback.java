package net.androidseminar.remoting;

public interface RemoteCallback<T> {

	void handleResult(T result);

}

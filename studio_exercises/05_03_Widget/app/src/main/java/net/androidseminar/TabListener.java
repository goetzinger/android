package net.androidseminar;

import android.R;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class TabListener<T> implements android.app.ActionBar.TabListener {

	private Fragment fragment;
	private final Activity activity;
	private final Class<T> classOfFragment;
	private final String tagOfFragment;

	public TabListener(Activity activity, String tagOfFragment,
			Class<T> classOfFragment) {
		this.activity = activity;
		this.tagOfFragment = tagOfFragment;
		this.classOfFragment = classOfFragment;
	}

	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		if (fragment == null) {
			fragment = Fragment
					.instantiate(activity, classOfFragment.getName());
			ft.add(R.id.content, fragment, tagOfFragment);
		} else {
			ft.attach(fragment);
		}
	}

	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		if (fragment != null) {
			ft.detach(fragment);
		}
	}

	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// User selected the already selected tab. Usually do nothing.
	}

}

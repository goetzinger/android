package net.androidseminar.reservation;

import java.net.URI;

import net.androidseminar.remoting.RemoteCall;
import net.androidseminar.remoting.RemoteCallParameter;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.IBinder;

public class LoadAllReservationsService extends Service {
	RemoteCall call = new RemoteCall();

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int toReturn = super.onStartCommand(intent, flags, startId);
		if (!call.getStatus().equals(Status.RUNNING)) {
			call = new RemoteCall();
			URI uri = URI
					.create("http://10.0.27.2:8080/Restful_Server_Project/rest/reservation/all");
			RemoteCallParameter remoteCallParameter = new RemoteCallParameter();
			remoteCallParameter.uri = uri;
			remoteCallParameter.callback = new AllReservationsRemoteCallback(
					this);
			call.execute(remoteCallParameter);
		}
		return toReturn;
	}
}

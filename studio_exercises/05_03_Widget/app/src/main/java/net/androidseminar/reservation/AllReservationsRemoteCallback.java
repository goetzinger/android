package net.androidseminar.reservation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import net.androidseminar.remoting.RemoteCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class AllReservationsRemoteCallback implements RemoteCallback<String> {

	private final Context context;

	public AllReservationsRemoteCallback(Context context) {
		this.context = context;
	}

	public void handleResult(String result) {
		List<Reservation> reservations = readReservationsFromJsonString(result);
		String[] ids = new String[reservations.size()];
		add2ContentProvider(reservations, ids);

	}

	private void add2ContentProvider(List<Reservation> reservations,
			String[] ids) {
		Cursor reservationIdCursor = queryIdsOfReservationsExistingLocaly(
				reservations, ids);
		List<Integer> localyExistingReservationIds = extractLocalyExistingReservationIds(reservationIdCursor);
		deleteLocalyExistingReservations(reservations,
				localyExistingReservationIds);
		for (Reservation reservation2Add : reservations) {
			ContentValues values = new ContentValues();
			values.put("_id", reservation2Add.getId());
			values.put("fahrzeug", reservation2Add.getFahrzeug());
			values.put("filiale", reservation2Add.getFiliale());
			values.put("bis", reservation2Add.getBis().getTime());
			values.put("von", reservation2Add.getVon().getTime());
			context.getContentResolver().insert(
					ReservationContentProvider.CONTENT_URI, values);
		}
	}

	private void deleteLocalyExistingReservations(
			List<Reservation> reservations,
			List<Integer> localyExistingReservationIds) {
		for (Iterator<Reservation> iterator = reservations.iterator(); iterator
				.hasNext();) {
			Reservation reservation = iterator.next();
			if (localyExistingReservationIds.contains(reservation.getId()))
				iterator.remove();
		}
	}

	private List<Integer> extractLocalyExistingReservationIds(
			Cursor reservationIdCursor) {
		List<Integer> localyExistingReservations = new ArrayList<Integer>(
				reservationIdCursor.getCount());
		while (reservationIdCursor.moveToNext()) {
			int id = reservationIdCursor.getInt(0);
			localyExistingReservations.add(id);
		}
		return localyExistingReservations;
	}

	private Cursor queryIdsOfReservationsExistingLocaly(
			List<Reservation> reservations, String[] ids) {
		for (int i = 0; i < reservations.size(); i++) {
			Reservation reservation = reservations.get(i);
			ids[i] = String.valueOf(reservation.getId());
		}
		String selection = "_id IN (";
		for (int i = 0; i < ids.length; i++) {
			String string = ids[i];
			selection = selection.concat(string);
			if (i < ids.length - 1)
				selection = selection.concat(" ,");
		}
		selection = selection.concat(")");
		Cursor reservationIdCursor = context.getContentResolver().query(
				ReservationContentProvider.CONTENT_URI,
				new String[] { ReservationContentProvider.ID_COLUMN_NAME },
				selection, null, null);
		return reservationIdCursor;
	}

	private List<Reservation> readReservationsFromJsonString(String result) {
		List<Reservation> reservations = new LinkedList<Reservation>();
		try {
			JSONObject json = new JSONObject(result);
			JSONArray jsonArray = json.getJSONArray("reservation");
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject ob = jsonArray.getJSONObject(i);
				Reservation reservation = transfromJSONToReservation(ob);
				reservations.add(reservation);
			}
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return reservations;
	}

	private Reservation transfromJSONToReservation(
			JSONObject reservationAsJsonObject) {
		try {
			return transformJSONToReservationThrowExceptions(reservationAsJsonObject);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	private Reservation transformJSONToReservationThrowExceptions(
			JSONObject reservationAsJsonObject) throws JSONException,
			ParseException {
		int id = reservationAsJsonObject.getInt("id");
		String fahrzeug = reservationAsJsonObject.getString("fahrzeug");
		String filiale = reservationAsJsonObject.getString("filiale");
		String bisJsonDateString = reservationAsJsonObject.getString("bis");
		String[] spittedDate = bisJsonDateString.split("-");
		Calendar bisCalendar = Calendar.getInstance();
		bisCalendar.set(Calendar.YEAR, Integer.parseInt(spittedDate[0]));
		bisCalendar.set(Calendar.MONTH, Integer.parseInt(spittedDate[1]));
		bisCalendar.set(
				Calendar.DAY_OF_MONTH,
				Integer.parseInt(spittedDate[2].substring(0,
						spittedDate[2].indexOf("T"))));
		Date bis = bisCalendar.getTime();
		String vonJsonDateString = reservationAsJsonObject.getString("von");
		String[] spittedVon = vonJsonDateString.split("-");
		Calendar vonCalendar = Calendar.getInstance();
		vonCalendar.set(Calendar.YEAR, Integer.parseInt(spittedVon[0]));
		vonCalendar.set(Calendar.MONTH, Integer.parseInt(spittedVon[1]));
		vonCalendar.set(
				Calendar.DAY_OF_MONTH,
				Integer.parseInt(spittedVon[2].substring(0,
						spittedVon[2].indexOf("T"))));
		Date von = bisCalendar.getTime();

		Reservation res = new Reservation();
		res.setId(id);
		res.setFahrzeug(fahrzeug);
		res.setFiliale(filiale);
		res.setBis(bis);
		res.setVon(von);
		return res;
	}

}

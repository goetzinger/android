package net.androidseminar.reservation;

import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.database.Cursor;
import android.util.Log;
import android.widget.RemoteViews;

import net.androidseminar.remoting.R;

import java.util.Calendar;

public class NextReservationWidgetProvider extends AppWidgetProvider {

	private final java.text.DateFormat dateFormat = java.text.DateFormat
			.getDateInstance(java.text.DateFormat.SHORT);
	@Override
	public void onEnabled(Context context) {
		super.onEnabled(context);
		Log.d(this.getClass().getName(), "widget ADDED");
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onUpdate(context, appWidgetManager, appWidgetIds);
		Cursor cursor = context.getContentResolver().query(ReservationContentProvider.CONTENT_URI, new String[]{"fahrzeug","filiale","von","bis"},
				"von > ?",  new String[]{""+Calendar.getInstance().getTimeInMillis()}, "von ASC");
		if(cursor.moveToFirst()) {
			RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.next_reservation_widget_layout);
			views.setTextViewText(R.id.nextReservationDate, dateFormat.format(cursor.getLong(cursor.getColumnIndex("von"))));

			// Tell the AppWidgetManager to perform an update on the current app widget
			int appWidgetId = appWidgetIds[0];
			appWidgetManager.updateAppWidget(appWidgetId, views);
		}
		cursor.close();
		Log.d(this.getClass().getName(), "widget updated");
	}
}

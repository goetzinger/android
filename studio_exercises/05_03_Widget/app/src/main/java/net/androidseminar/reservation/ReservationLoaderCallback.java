package net.androidseminar.reservation;

import android.app.ListFragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.CursorAdapter;

public class ReservationLoaderCallback implements LoaderCallbacks<Cursor> {

	private final ListFragment context;

	public ReservationLoaderCallback(ListFragment c) {
		this.context = c;
	}

	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(context.getActivity(),
				ReservationContentProvider.CONTENT_URI, null,
				args.getString("selection"),
				args.getStringArray("selectionArgs"), null);
	}

	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		((CursorAdapter) context.getListAdapter()).swapCursor(data);
		context.getActivity().startManagingCursor(data);
		if (context.isResumed()) {
			context.setListShown(true);
		} else
			context.setListShownNoAnimation(true);

	}

	public void onLoaderReset(Loader<Cursor> loader) {
		((CursorAdapter) context.getListAdapter()).swapCursor(null);
	}

}

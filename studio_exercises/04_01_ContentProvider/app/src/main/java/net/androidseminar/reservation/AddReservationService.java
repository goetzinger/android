package net.androidseminar.reservation;

import java.io.Serializable;

import net.androidseminar.reservation.ReservationDAOJDBCService.LocalBinder;
import android.app.Service;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

public class AddReservationService extends Service {

	private ReservationDAOJDBCService fileDAOService;
	private Reservation toAdd;
	private final ServiceConnection con = new ServiceConnection() {

		public void onServiceDisconnected(ComponentName name) {
			fileDAOService = null;
		}

		public void onServiceConnected(ComponentName name, IBinder service) {
			fileDAOService = ((LocalBinder) service).getService();
			fileDAOService.addReservations(toAdd);
			AddReservationService.this.stopSelf();
		}
	};

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();

	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int returnFromSuper = super.onStartCommand(intent, flags, startId);
		Bundle bundle = intent.getExtras();
		Serializable serializable = bundle.getSerializable("reservation");
		if (serializable instanceof Reservation) {
			toAdd = (Reservation) serializable;
			Log.i(this.getClass().getName(), toAdd.toString());
			bindService(new Intent(this, ReservationDAOJDBCService.class), con,
					Context.BIND_AUTO_CREATE);
		}

		return returnFromSuper;
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		unbindService(con);
	}
}

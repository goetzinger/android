package net.androidseminar.reservation;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

public class ReservationDAOJDBCService extends Service {

	private final List<Reservation> reservationsCache = new LinkedList<Reservation>();
	private ReservationSQLiteOpenHelper dbHandler;

	@Override
	public void onCreate() {
		super.onCreate();
		dbHandler = new ReservationSQLiteOpenHelper(this);
		readAll();
	}

	private void readAll() {
		reservationsCache.addAll(dbHandler.getAllReservations());
	}

	@Override
	public IBinder onBind(Intent intent) {
		return new LocalBinder();
	}

	public List<Reservation> getAllReservations() {
		ArrayList<Reservation> copy = new ArrayList<Reservation>(
				reservationsCache.size());
		copy.addAll(reservationsCache);
		return copy;
	}

	public void addReservations(Reservation toAdd) {
		this.reservationsCache.add(toAdd);
		insertIntoDB(toAdd);
	}

	private void insertIntoDB(Reservation toAdd) {
		dbHandler.insertReservation(toAdd);
	}

	class LocalBinder extends Binder {

		ReservationDAOJDBCService getService() {
			return ReservationDAOJDBCService.this;
		}
	}
}

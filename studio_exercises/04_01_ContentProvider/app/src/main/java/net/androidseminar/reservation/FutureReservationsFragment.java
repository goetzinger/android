package net.androidseminar.reservation;

import android.os.Bundle;

public class FutureReservationsFragment extends AllReservationsFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
	}

	@Override
	protected ReservationListAdapter getAdapter() {
		return new ReservationListAdapter(getActivity());
	}

}

package com.example.goetzingert.tag_01_02_intents;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DynamicActivity extends Activity {
    @Override
    protected void onCreate(@androidx.annotation.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        LinearLayout panel = new LinearLayout(this);
        panel.setLayoutParams(new LinearLayout.LayoutParams(100,100));
        TextView textView = new TextView(this);
        textView.setText("SOME TEXT");
        panel.addView(textView,0);
        setContentView();

    }
}

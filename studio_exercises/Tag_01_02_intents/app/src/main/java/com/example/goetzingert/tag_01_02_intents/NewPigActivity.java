package com.example.goetzingert.tag_01_02_intents;

import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class NewPigActivity extends AppCompatActivity {

    public static final String NEW_PIG_ACTION = "net.androidseminar.new_pig";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_pig);
    }

    public void save(View button) {
        insertInDB();
        notifyAboutInsert();
        finish();
    }

    private void insertInDB() {
        EditText weightField = findViewById(R.id.weightInput);
        EditText nameField = findViewById(R.id.nameInput);
        ContentValues values = new ContentValues();
        values.put("name", nameField.getText().toString());
        values.put("weight", Integer.parseInt(weightField.getText().toString()));
        Uri insert = getContentResolver().insert(MyContentProvider.CONTENT_URI, values);
    }

    private void notifyAboutInsert() {
        sendBroadcast(new Intent(NEW_PIG_ACTION));
    }

    public void kaputtMachen(View v) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    asynchronousToUIThread();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }

            private void asynchronousToUIThread() throws InterruptedException {
                //Running inside Thread xyz != UI Thread
                Thread.sleep(3000);
                EditText nameField = findViewById(R.id.nameInput);
                try {
                    nameField.getHandler().post(new Runnable() {
                        @Override
                        public void run() {
                            synchronImUIThread();

                        }

                        private void synchronImUIThread() {
                            //Runns on UI Thread
                            nameField.setText("Hallo du");
                        }
                    });
                } catch (Throwable t) {
                    t.printStackTrace();
                }
            }
        }).start();
    }
}

package com.example.goetzingert.tag_01_02_intents;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.TabWidget;
import android.widget.TableLayout;
import android.widget.Toast;

public class AllPigsActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_pigs);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater from = getMenuInflater();
        from.inflate(R.menu.pig_activity_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String inernationalizedAddItemTitle = getString(R.string.add);
        if (item.getTitle().equals(inernationalizedAddItemTitle)) {
            startActivity(new Intent(this, NewPigActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }



}

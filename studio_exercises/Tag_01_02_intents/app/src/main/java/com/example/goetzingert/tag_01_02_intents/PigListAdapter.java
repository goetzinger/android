package com.example.goetzingert.tag_01_02_intents;

import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

class PigListAdapter extends BaseAdapter {


    private List<Pig> toShow = new ArrayList<>();

    public PigListAdapter() {
        for (int i = 0; i < 100; i++) {
            toShow.add(new Pig("Schwein " + i, i * 10));
        }
    }

    @Override
    public int getCount() {
        return toShow.size();
    }

    @Override
    public Object getItem(int position) {
        return toShow.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.listitem, null);
        }

        Pig pigAtPosition = toShow.get(position);
        TextView nameView = convertView.findViewById(R.id.nameView);
        nameView.setText(pigAtPosition.getName());
        return convertView;
    }



}

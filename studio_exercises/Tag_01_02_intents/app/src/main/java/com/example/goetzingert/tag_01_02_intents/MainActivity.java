package com.example.goetzingert.tag_01_02_intents;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button startB = findViewById(R.id.startB);
        NotificationManagerCompat manager = NotificationManagerCompat.from(this);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
        builder.setContentText("Hallo ich bin eine notification");
        builder.setContentTitle("TITLE OF NOT");

        builder.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, AllPigsActivity.class),PendingIntent.FLAG_UPDATE_CURRENT));
        Notification not = (Notification) builder.build();
        manager.notify(112,not);
        startB.setOnClickListener(this::startOtherActivity);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater from = getMenuInflater();
        from.inflate(R.menu.pig_activity_menu,menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        String inernationalizedAddItemTitle = getString(R.string.add);
        if(item.getTitle().equals(inernationalizedAddItemTitle)){
            Toast.makeText(this, "Add geklicked", Toast.LENGTH_LONG).show();
        }
        return super.onOptionsItemSelected(item);
    }



    public void startOtherActivity(View v){

        //Vorsicht: OtherActivity in manifest?
        Intent intent = new Intent(this, AllPigsActivity.class);
        intent.putExtra("message","Dies ist die Nachricht!");
        startActivity(intent);

    }
}

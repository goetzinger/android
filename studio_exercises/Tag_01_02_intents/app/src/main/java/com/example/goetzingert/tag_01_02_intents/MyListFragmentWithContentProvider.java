package com.example.goetzingert.tag_01_02_intents;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.widget.CursorAdapter;

public class MyListFragmentWithContentProvider extends ListFragment {

    BroadcastReceiver refreshViewAfterInsert = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            LoaderManager.getInstance(MyListFragmentWithContentProvider.this).restartLoader(0, new Bundle(), new PigCursorLoaderCallback());
        }
    };


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new PigCursorAdapter<Pig>(this.getContext(), null));
        startLoader();
        getActivity().registerReceiver(refreshViewAfterInsert, new IntentFilter(NewPigActivity.NEW_PIG_ACTION));
    }

    private void startLoader() {
        LoaderManager.getInstance(this).initLoader(0, new Bundle(), new PigCursorLoaderCallback());
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unregisterReceiver(refreshViewAfterInsert);
    }

    @Override
    public void onStart() {
        super.onStart();
        setEmptyText("Wird gerade geladen");
    }

    private class PigCursorLoaderCallback implements LoaderManager.LoaderCallbacks<Cursor> {
        @NonNull
        @Override
        public Loader onCreateLoader(int i, @Nullable Bundle bundle) {
            return new CursorLoader(getContext(), MyContentProvider.CONTENT_URI, new String[]{"_id", "name", "weight"}, null, null, null);
        }

        @Override
        public void onLoadFinished(@NonNull Loader loader, Cursor o) {
            ((CursorAdapter) MyListFragmentWithContentProvider.this.getListAdapter()).swapCursor(o);
        }

        @Override
        public void onLoaderReset(@NonNull Loader loader) {
            ((CursorAdapter) MyListFragmentWithContentProvider.this.getListAdapter()).swapCursor(null);

        }
    }
}

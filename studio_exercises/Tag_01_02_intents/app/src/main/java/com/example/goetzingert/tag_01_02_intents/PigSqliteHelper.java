package com.example.goetzingert.tag_01_02_intents;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class PigSqliteHelper extends SQLiteOpenHelper {

    private static final int VERSION = 3;

    public PigSqliteHelper(Context context){
        super(context, "Pig",null, VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE Pig ( _id INTEGER, name TEXT, weight INTEGER)");
        db.execSQL("INSERT INTO Pig (_id, name, weight) VALUES (1, 'Miss Piggy', 100)");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }
}

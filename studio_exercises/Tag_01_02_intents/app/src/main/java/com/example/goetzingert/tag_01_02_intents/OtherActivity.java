package com.example.goetzingert.tag_01_02_intents;

import android.app.Activity;
import android.os.Bundle;
import android.widget.Toast;

public class OtherActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otherlayout);
        Bundle extras = getIntent().getExtras();
        String messageFromSourceOfIntent = extras.getString("message");
        Toast.makeText(this, messageFromSourceOfIntent, Toast.LENGTH_LONG).show();
    }
}

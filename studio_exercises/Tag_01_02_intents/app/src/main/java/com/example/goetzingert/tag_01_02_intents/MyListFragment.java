package com.example.goetzingert.tag_01_02_intents;

import android.app.Service;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v4.app.ListFragment;
import android.widget.ArrayAdapter;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyListFragment extends ListFragment {

    private PigDAOService service;

    //callback wenn bindung da / weg
    private ServiceConnection connection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyListFragment.this.service = ((PigDAOService.LocalBinder)service).daoService();
            ((ArrayAdapter) getListAdapter()).addAll(MyListFragment.this.service.toArray());
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {

        }
    };

    @Override
    public void onResume() {
        super.onResume();
        ((ArrayAdapter) getListAdapter()).clear();
        ((ArrayAdapter) getListAdapter()).addAll(this.service.toArray());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setListAdapter(new PigListAdapterEasy(this.getContext(), R.layout.listitem, R.id.nameView,
                new ArrayList()));

        getActivity().bindService(new Intent(getContext(), PigDAOService.class),connection, Service.BIND_AUTO_CREATE);

        Intent service = new Intent(getContext(), PigDAOService.class);
        service.putExtra("name", "Schweinchen Dick");
        service.putExtra("weight",100);
        getActivity().startService(service);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        getActivity().unbindService(connection);
    }

    @Override
    public void onStart() {
        super.onStart();
        setEmptyText("Wird gerade geladen");
    }
}

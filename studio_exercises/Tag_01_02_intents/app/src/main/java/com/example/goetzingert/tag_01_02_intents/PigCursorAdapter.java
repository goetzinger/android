package com.example.goetzingert.tag_01_02_intents;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CursorAdapter;
import android.widget.TextView;

import java.util.List;

class PigCursorAdapter<Pig> extends CursorAdapter {


    public PigCursorAdapter(Context context, Cursor c) {
        super(context, c);
    }


    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View v = inflater.inflate(R.layout.listitem, null);
        TextView viewById = v.findViewById(R.id.textView);
        viewById.setText(String.format("Schwein: {name : %s, weight : %s}",
                cursor.getString(cursor.getColumnIndex("name")),
                        cursor.getInt(cursor.getColumnIndex("weight"))));
        return v;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView viewById = view.findViewById(R.id.textView);
        viewById.setText(String.format("Schwein: {name : %s, weight : %s}",
                cursor.getString(cursor.getColumnIndex("name")),
                cursor.getInt(cursor.getColumnIndex("weight"))));

    }
}

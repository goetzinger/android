package com.example.goetzingert.tag_01_02_intents;

import android.app.Service;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;

import java.io.FileDescriptor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PigDAOService extends Service {

    private List<Pig> inhabitants = new ArrayList<>();

    private static final String[] COULUMNS = {"name","weight"};
    private PigSqliteHelper helper;

    public PigDAOService() {
        inhabitants.add(new Pig("MissPiggy", 299));

    }


    @Override
    public void onCreate() {
        super.onCreate();
        helper = new PigSqliteHelper(this);
        getAllPigs();
    }

    private void getAllPigs() {
        SQLiteDatabase readableDatabase =
                helper.getReadableDatabase();
        Cursor cursor = readableDatabase.rawQuery("SELECT * FROM Pig pig", null);
        while(cursor.moveToNext())
        {
            String name = cursor.getString(cursor.getColumnIndex("name"));
            int weight = cursor.getInt(cursor.getColumnIndex("weight"));
            inhabitants.add(new Pig(name, weight));
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        helper.close();
    }

    public Pig[] toArray() {
        return inhabitants.toArray(new Pig[]{});
    }

    public boolean add(Pig pig) {
        SQLiteDatabase writableDatabase = helper.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("name",pig.getName());
        values.put("weight", pig.getWeight());
        writableDatabase.insert("Pig", null, values);
        return inhabitants.add(pig);
    }

    public Pig remove(int index) {
        return inhabitants.remove(index);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return new LocalBinder() {


        };
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Bundle extras = intent.getExtras();
        String name = extras.getString("name");
        int weight = extras.getInt("weight");
        inhabitants.add(new Pig(name, weight));
        return START_NOT_STICKY;
    }

    public class LocalBinder extends Binder {

        public PigDAOService daoService() {
            return PigDAOService.this;
        }
    }
}

package com.example.goetzingert.sender;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

public class Counter {

    private static final String PREFS_NAME = "ble_example";
    private static final String PREFS_KEY_COUNTER = "counter";

    private final SharedPreferences prefs;

    public Counter(Context context) {
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    }

    public int getCounterValue() {
        return prefs.getInt(PREFS_KEY_COUNTER, 0);
    }

    @SuppressLint("ApplySharedPref")
    public int incrementCounterValue() {
        int newValue = getCounterValue() + 1;
        prefs.edit().putInt(PREFS_KEY_COUNTER, newValue).commit();
        return newValue;
    }
}

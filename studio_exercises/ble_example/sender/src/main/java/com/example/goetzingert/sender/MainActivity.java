package com.example.goetzingert.sender;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.goetzingert.ble_example.common.Ints;

public class MainActivity extends AppCompatActivity {

    private GattServer gattServer;
    private Counter counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gattServer = new GattServer();
        gattServer.onCreate(this, new GattServer.GattServerListener() {
            @Override
            public byte[] onCounterRead() {
                return Ints.toByteArray(counter.getCounterValue());
            }

            @Override
            public void onInteractorWritten() {
                int count = counter.incrementCounterValue();
                ((TextView)findViewById(R.id.counter)).setText(""+count);

            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        gattServer.onDestroy();
    }
}

package net.androidseminar.reservation;

import java.io.Serializable;
import java.util.Date;

@SuppressWarnings("serial")
public class Reservation implements Comparable<Reservation>, Serializable {

	private String fahrzeug;

	private String filiale;

	private Date von;

	private Date bis;

	public String getFahrzeug() {
		return fahrzeug;
	}

	public void setFahrzeug(String fahrzeug) {
		this.fahrzeug = fahrzeug;
	}

	public String getFiliale() {
		return filiale;
	}

	public void setFiliale(String filiale) {
		this.filiale = filiale;
	}

	public Date getVon() {
		return von;
	}

	public void setVon(Date von) {
		this.von = von;
	}

	public Date getBis() {
		return bis;
	}

	public void setBis(Date bis) {
		this.bis = bis;
	}

	public int compareTo(Reservation another) {
		return this.von.compareTo(another.von);
	}

	@Override
	public String toString() {
		return "Reservation [fahrzeug=" + fahrzeug + ", filiale=" + filiale
				+ ", von=" + von + ", bis=" + bis + "]";
	}

}

package net.androidseminar.reservation;

import java.io.Serializable;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;

/**
 * Service. Don't forget to list in AndroidManfiest
 * 
 * @author goetzingert
 * 
 */
public class AddReservationService extends Service {

	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	@Override
	public void onCreate() {
		super.onCreate();
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int returnFromSuper = super.onStartCommand(intent, flags, startId);

		// get Extras from intent. The Activity/Service which starts this
		// service put some data in the intent
		Bundle bundle = intent.getExtras();
		Serializable serializable = bundle.getSerializable("reservation");
		if (serializable instanceof Reservation) {
			Reservation reservation2Add = (Reservation) serializable;
			Log.i(this.getClass().getName(), reservation2Add.toString());
		}
		this.stopSelf();
		return returnFromSuper;
	}
}

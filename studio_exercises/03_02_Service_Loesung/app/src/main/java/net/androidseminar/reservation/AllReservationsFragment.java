package net.androidseminar.reservation;

import net.androidseminar.intent.loesung.R;
import android.app.ListFragment;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.PreferenceManager;

public class AllReservationsFragment extends ListFragment {

	private final OnSharedPreferenceChangeListener listener = new PreferencesListener();
	private ReservationListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new ReservationListAdapter(this.getActivity()
				.getApplicationContext());
		setListAdapter(adapter);
		PreferenceManager.getDefaultSharedPreferences(
				this.getActivity().getApplicationContext())
				.registerOnSharedPreferenceChangeListener(listener);
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		PreferenceManager.getDefaultSharedPreferences(
				getActivity().getApplicationContext())
				.unregisterOnSharedPreferenceChangeListener(listener);
	}

	class PreferencesListener implements OnSharedPreferenceChangeListener {

		public void onSharedPreferenceChanged(
				SharedPreferences sharedPreferences, String key) {
			String sortationKey = getResources().getString(
					R.string.sortResASCKey);
			if (key.equals(sortationKey))
				adapter.sort(sharedPreferences, sortationKey);
		}
	}

}

package net.androidseminar.reservation;

import net.androidseminar.intent.loesung.R;
import android.os.Bundle;
import android.preference.PreferenceActivity;

public class ReservationPreferenceActivity extends PreferenceActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		addPreferencesFromResource(R.xml.mainprefs);
	}

}

package net.androidseminar.layout;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

/**
 * Activity has to be listed in Androids Manifest File
 * 
 * @author goetzingert
 * 
 */
public class LayoutActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// Sets the content part of the Activity by referencing a layout-file
		setContentView(R.layout.main);
		Log.d(this.getClass().getSimpleName(), "Login created!");
	}

}
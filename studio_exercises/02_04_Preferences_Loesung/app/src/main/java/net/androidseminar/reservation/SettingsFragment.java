package net.androidseminar.reservation;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import net.androidseminar.intent.loesung.R;

public class SettingsFragment extends PreferenceFragment {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Load the preferences from an XML resource
        addPreferencesFromResource(R.xml.mainprefs);
    }
}

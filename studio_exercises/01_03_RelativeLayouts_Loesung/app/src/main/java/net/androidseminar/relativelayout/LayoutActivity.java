package net.androidseminar.relativelayout;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class LayoutActivity extends Activity {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Sets the content part of the Activity by referencing a layout-file
		setContentView(R.layout.main);
		Log.d(this.getClass().getSimpleName(), "Login created!");
		Button button = findViewById(R.id.button_login);
		button.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Toast.makeText(view.getContext(), "Hallo Welt", Toast.LENGTH_LONG).show();
			}
		});

	}

}
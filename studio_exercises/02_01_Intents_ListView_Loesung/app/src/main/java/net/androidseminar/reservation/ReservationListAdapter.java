package net.androidseminar.reservation;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import net.androidseminar.intent.loesung.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

class ReservationListAdapter extends BaseAdapter {

	private final List<Reservation> reservations = new LinkedList<Reservation>();
	private final Context context;
	private final java.text.DateFormat dateFormat = java.text.DateFormat
			.getDateInstance(java.text.DateFormat.SHORT);

	ReservationListAdapter(Context context) {
		this.context = context;
		for (int i = 0; i < 10; i++) {
			Reservation r = new Reservation();
			r.setFahrzeug("Fahrzeug " + i);
			r.setFiliale("Filiale " + i);
			Calendar von = Calendar.getInstance();
			von.roll(Calendar.DAY_OF_YEAR, 0 - i);
			r.setVon(von.getTime());
			Calendar bis = Calendar.getInstance();
			bis.roll(Calendar.DAY_OF_YEAR, 0 + i);
			r.setBis(bis.getTime());
			reservations.add(r);
		}
	}

	public int getCount() {
		return reservations.size();
	}

	public Object getItem(int position) {
		return reservations.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		Reservation reservation2Display = reservations.get(position);

		View v;
		if (convertView == null) {

			LayoutInflater inflater = LayoutInflater.from(context);
			v = inflater.inflate(R.layout.reservationlistentry, null, false);
		} else
			v = convertView;

		TextView fahrzeugText = ((TextView) v.findViewById(R.id.fahrzeugText));
		fahrzeugText.setText(reservation2Display.getFahrzeug());
		TextView filialeText = ((TextView) v.findViewById(R.id.filialeText));
		filialeText.setText(reservation2Display.getFiliale());
		TextView vonText = ((TextView) v.findViewById(R.id.vonText));
		vonText.setText(dateFormat.format(reservation2Display.getVon()));
		TextView bisText = ((TextView) v.findViewById(R.id.bisText));
		bisText.setText(dateFormat.format(reservation2Display.getBis()));

		return v;
	}

}

package net.androidseminar.reservation;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.ListAdapter;

public class AllReservationsActivity extends ListActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ListAdapter adapter = new ReservationListAdapter(
				this.getApplicationContext());
		setListAdapter(adapter);
		// getting Data shared by the Source of the intent
		super.getActionBar().setTitle(
				getIntent().getExtras().getCharSequence("username"));
	}

}

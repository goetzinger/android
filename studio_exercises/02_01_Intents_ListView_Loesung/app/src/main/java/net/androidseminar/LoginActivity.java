package net.androidseminar;

import net.androidseminar.intent.loesung.R;
import net.androidseminar.reservation.AllReservationsActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class LoginActivity extends Activity {
	/** Called when the activity is first created. */

	static String simpleName = LoginActivity.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Log.d(simpleName, "Login created!");
		Button loginButton = (Button) super.findViewById(R.id.button_login);
		loginButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				// Creating Intent with Source LoginActivity and Destination
				// AllReservationsActiviy
				Intent intent = new Intent(LoginActivity.this,
						AllReservationsActivity.class);

				// Put some data, to share with the Destination Activity
				intent.putExtra("username",
						((EditText) findViewById(R.id.username)).getText());

				// start Destination Activity
				v.getContext().startActivity(intent);
			}
		});

	}
}
package net.androidseminar.reservation;

import android.app.ListFragment;
import android.os.Bundle;
import android.widget.ListAdapter;

/**
 * Used in allreservations layout file as part of the activities content pane.
 * 
 * @author goetzingert
 * 
 */
public class AllReservationsFragment extends ListFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ListAdapter adapter = new ReservationListAdapter(this.getActivity()
				.getApplicationContext());
		setListAdapter(adapter);
	}

}

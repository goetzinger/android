package net.androidseminar.reservation;

import net.androidseminar.intent.loesung.R;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class AllReservationsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.allreservations);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		// read reservations menu file and use it as the menu for the activity
		inflater.inflate(R.menu.reservations, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// if NEW Item is selected start Activity
		if (item.getItemId() == R.id.item1) {
			startActivity(new Intent(this, NewReservationActivity.class));
			return true;
		}
		return false;
	}
}

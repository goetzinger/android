package net.androidseminar.eventhandling;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

import java.util.function.Function;

public class LoginActivity extends Activity {
	/** Called when the activity is first created. */

	static String simpleName = LoginActivity.class.getSimpleName();

	private EditText username;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Log.d(simpleName, "Login created!");
		// Lookup Button by ID
		Button loginButton = (Button) super.findViewById(R.id.button_login);
		username = findViewById(R.id.username);

		// Setting ClickListener
		loginButton.setOnClickListener(new OnClickListener() {

			/**
			 * Method Invoked by when user clicks button
			 * 
			 * @see View.OnClickListener#onClick
			 */
			public void onClick(View v) {
				boolean b = username.getText().length() == 0;
				Log.i(simpleName, "Login pressed!");
			}
		});

		loginButton.setOnClickListener(this::click);
	}

	public void click(View v){

	}
}
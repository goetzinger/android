package net.androidseminar.reservation;

import net.androidseminar.loader.R;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;

public class AllReservationsFragment extends ListFragment {

	private ReservationListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		// initialize Loader. ReservationLoaderCallback will be called asynchron
		this.getLoaderManager().initLoader(0, new Bundle(),
				new ReservationLoaderCallback(this));

		// Adapter with null cursor, because data will be retreived by loader
		adapter = new ReservationListAdapter(getActivity(), null);

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		// Setting Adapter
		setListAdapter(adapter);

	}

	/**
	 * Called after {@link #onViewCreated(View, Bundle)}
	 */
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// hide list show Progress
		setListShown(false);
		// Set String when no entry is present
		setEmptyText(getResources().getString(R.string.noContentInList));
	}

}

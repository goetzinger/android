package net.androidseminar.reservation;

import net.androidseminar.TabListener;
import net.androidseminar.loader.R;
import android.app.ActionBar;
import android.app.ActionBar.Tab;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

public class AllReservationsActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.allreservations);
		ActionBar actionBar = getActionBar();
		// The Tab Entries will be navigable
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
		// show no title
		actionBar.setDisplayShowTitleEnabled(false);

		// Creating, Adding Tab and setting TabListener which receives
		// "Tab selected" Event from user
		Tab tab = actionBar
				.newTab()
				.setText(R.string.allReservations)
				.setTabListener(
						new TabListener<AllReservationsFragment>(this, "all",
								AllReservationsFragment.class));
		actionBar.addTab(tab);

		// Creating, Adding 2nd Tab and setting TabListener which receives
		// "Tab selected" Event from user
		tab = actionBar
				.newTab()
				.setText(R.string.futureReservations)
				.setTabListener(
						new TabListener<FutureReservationsFragment>(this,
								"future", FutureReservationsFragment.class));
		actionBar.addTab(tab);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.reservations, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.newReservationItem:
			startActivity(new Intent(this, NewReservationActivity.class));
			return true;
		case R.id.preferencesItem:
			startActivity(new Intent(this, ReservationPreferenceActivity.class));
			return true;
		}
		return false;
	}

}

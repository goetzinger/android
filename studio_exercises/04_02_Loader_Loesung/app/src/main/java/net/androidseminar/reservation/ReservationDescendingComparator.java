package net.androidseminar.reservation;

import java.util.Comparator;

public class ReservationDescendingComparator implements Comparator<Reservation> {

	public int compare(Reservation lhs, Reservation rhs) {
		return lhs.getVon().compareTo(rhs.getVon()) * -1;
	}

}

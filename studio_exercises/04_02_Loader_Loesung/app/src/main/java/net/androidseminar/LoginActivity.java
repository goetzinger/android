package net.androidseminar;

import net.androidseminar.loader.R;
import net.androidseminar.reservation.AllReservationsActivity;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class LoginActivity extends Activity {
	/** Called when the activity is first created. */

	static String simpleName = LoginActivity.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);

		Log.d(simpleName, "Login created!");
		Button loginButton = (Button) super.findViewById(R.id.button_login);
		loginButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				v.getContext().startActivity(
						new Intent(LoginActivity.this,
								AllReservationsActivity.class));
			}
		});
	}
}
package net.androidseminar;

import android.app.ActionBar.Tab;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;

public class TabListener<T extends Fragment> implements android.app.ActionBar.TabListener {

	private Fragment fragment;
	private final Activity activity;
	private final Class<T> classOfFragment;
	private final String tagOfFragment;

	public TabListener(Activity activity, String tagOfFragment,
			Class<T> classOfFragment) {
		this.activity = activity;
		this.tagOfFragment = tagOfFragment;
		this.classOfFragment = classOfFragment;
	}

	/**
	 * Called if user switches the tabs by selecting one in the action bar
	 */
	public void onTabSelected(Tab tab, FragmentTransaction ft) {
		if (fragment == null) {
			fragment = Fragment
					.instantiate(activity, classOfFragment.getName());
			// Add the tab to the content view of the current activity
			ft.add(android.R.id.content, fragment, tagOfFragment);
		} else {
			// if it has already been added, only attach it at the stored
			// position.
			ft.attach(fragment);
		}
	}

	/**
	 * Called if user switches the tabs by selecting one in the action bar, the
	 * current tab gets unselected. The TabListener of another tab will receive
	 * an {@link TabListener#onTabSelected(Tab, FragmentTransaction)} event
	 */
	public void onTabUnselected(Tab tab, FragmentTransaction ft) {
		// Cause another tab is selected, detach this one from view
		if (fragment != null) {
			ft.detach(fragment);
		}
	}

	public void onTabReselected(Tab tab, FragmentTransaction ft) {
		// User selected the already selected tab. Usually do nothing.
	}

}

package net.androidseminar.reservation;

import java.util.Calendar;
import java.util.Iterator;

import android.content.Context;

public class ReservationFutureListAdapter extends ReservationListAdapter {

	ReservationFutureListAdapter(Context context) {
		super(context);
		for (Iterator<Reservation> iterator = reservations.iterator(); iterator
				.hasNext();) {
			Reservation res = iterator.next();
			if (res.getVon().before(Calendar.getInstance().getTime())) {
				iterator.remove();
			}

		}
	}

}

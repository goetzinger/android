package net.androidseminar.reservation;

import android.app.ListFragment;
import android.os.Bundle;
import android.widget.ListAdapter;

public class AllReservationsFragment extends ListFragment {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ListAdapter adapter = new ReservationListAdapter(this.getActivity()
				.getApplicationContext());
		setListAdapter(adapter);
	}

}

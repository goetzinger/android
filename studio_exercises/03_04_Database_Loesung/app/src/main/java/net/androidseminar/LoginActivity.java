package net.androidseminar;

import net.androidseminar.intent.loesung.R;
import net.androidseminar.reservation.AllReservationsActivity;
import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class LoginActivity extends Activity {
	/** Called when the activity is first created. */

	static String simpleName = LoginActivity.class.getSimpleName();

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		NotificationManager manager = NotificationCompat.
		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		builder.setContentText("Hallo ich bin eine notification");
		builder.setContentTitle("TITLE OF NOT");

		builder.setContentIntent(PendingIntent.getActivity(this, 0, new Intent(this, AllReservationsActivity.class),PendingIntent.FLAG_UPDATE_CURRENT));
		Notification not = (Notification) builder.build();
		manager.notify(0,not);
		Log.d(simpleName, "Login created!");
		Button loginButton = (Button) super.findViewById(R.id.button_login);
		loginButton.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				v.getContext().startActivity(
						new Intent(LoginActivity.this,
								AllReservationsActivity.class));
			}
		});
	}
}
package net.androidseminar.reservation;

import java.net.URI;

import net.androidseminar.remoting.RemoteCall;
import net.androidseminar.remoting.RemoteCallParameter;
import android.app.Service;
import android.content.Intent;
import android.os.AsyncTask.Status;
import android.os.IBinder;

public class LoadAllReservationsService extends Service {
	RemoteCall call = new RemoteCall();

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Starts the {@link RemoteCall} that makes a remotecall to a restful
	 * ReservationService. The method passes a
	 * {@link AllReservationsRemoteCallback} that takes the result, convert it
	 * to Reservations Objects and put them into the database
	 */
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		int toReturn = super.onStartCommand(intent, flags, startId);
		if (!call.getStatus().equals(Status.RUNNING)) {
			call = new RemoteCall();
			URI uri = URI
					.create("http://10.0.2.2:8080/reservation");
			// As we use different Paramter Types (URI, Callback) we have to
			// introduce a wrapper object
			RemoteCallParameter<String> remoteCallParameter = new RemoteCallParameter<String>();
			remoteCallParameter.uri = uri;
			// After remote call finishs network call this object should handle
			// the data received.
			// It adds all data to the contentprovider which notifies all
			// listener
			remoteCallParameter.callback = new AllReservationsRemoteCallback(
					this);
			// Execute the async tasks RemoteCall.doInBackground
			call.execute(remoteCallParameter);
		}
		return toReturn;
	}
}

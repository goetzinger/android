package net.androidseminar.remoting;

/**
 * Interface for all classes that handle results of a remote call.
 * 
 * @author goetzingert
 * 
 * @param <T>
 *            Type of the RemoteCall Result
 */
public interface RemoteCallback<T> {

	void handleResult(T result);

}

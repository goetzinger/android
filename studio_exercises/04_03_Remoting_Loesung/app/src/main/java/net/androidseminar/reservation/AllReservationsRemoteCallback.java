package net.androidseminar.reservation;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import net.androidseminar.remoting.RemoteCall;
import net.androidseminar.remoting.RemoteCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

/**
 * Called by the {@link RemoteCall} after the httpResponse is decoded to a
 * jsonString. This class converts the string to Reservation Objects and pass
 * the new ones to the contentProvider (database). In order to find out the
 * local not existing Reservations it queries the id of the local ones.
 * 
 * @author goetzingert
 * 
 */
public class AllReservationsRemoteCallback implements RemoteCallback<String> {

	private final Context context;

	public AllReservationsRemoteCallback(Context context) {
		this.context = context;
	}

	public void handleResult(String result) {

		// Read all Reservations from jsonString
		List<Reservation> reservations = readReservationsFromJsonString(result);
		// add the new one to the contentProvider
		addNewReservations2ContentProvider(reservations);

	}

	/**
	 * Converts the jsonString to Reservationobjects and adds all to a list
	 * 
	 * @param result
	 * @return
	 */
	private List<Reservation> readReservationsFromJsonString(String result) {
		List<Reservation> reservations = new LinkedList<Reservation>();
		try {
			// Read complete jsonString
			JSONArray jsonArray = new JSONArray(result);
			// Step to reservationList

			for (int i = 0; i < jsonArray.length(); i++) {
				// Next Reservation entry
				JSONObject ob = jsonArray.getJSONObject(i);
				Reservation reservation = transfromJSONToReservation(ob);
				reservations.add(reservation);
			}
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		return reservations;
	}

	// catch exception and rethrow
	private Reservation transfromJSONToReservation(
			JSONObject reservationAsJsonObject) {
		try {
			return transformJSONToReservationThrowExceptions(reservationAsJsonObject);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * transforms one Reservation JSon object to a real Reservation object and
	 * return
	 * 
	 * @param reservationAsJsonObject
	 *            The next String part to Convert
	 * @return
	 * @throws JSONException
	 * @throws ParseException
	 */
	private Reservation transformJSONToReservationThrowExceptions(
			JSONObject reservationAsJsonObject) throws JSONException,
			ParseException {
		int id = reservationAsJsonObject.getInt("id");
		String fahrzeug = reservationAsJsonObject.getString("fahrzeug");
		String filiale = reservationAsJsonObject.getString("filiale");
		String bisJsonDateString = reservationAsJsonObject.getString("bis");
		String[] spittedDate = bisJsonDateString.split("-");
		Calendar bisCalendar = Calendar.getInstance();
		bisCalendar.set(Calendar.YEAR, Integer.parseInt(spittedDate[0]));
		bisCalendar.set(Calendar.MONTH, Integer.parseInt(spittedDate[1]));
		bisCalendar.set(
				Calendar.DAY_OF_MONTH,
				Integer.parseInt(spittedDate[2].substring(0,
						spittedDate[2].indexOf("T"))));
		Date bis = bisCalendar.getTime();
		String vonJsonDateString = reservationAsJsonObject.getString("von");
		String[] spittedVon = vonJsonDateString.split("-");
		Calendar vonCalendar = Calendar.getInstance();
		vonCalendar.set(Calendar.YEAR, Integer.parseInt(spittedVon[0]));
		vonCalendar.set(Calendar.MONTH, Integer.parseInt(spittedVon[1]));
		vonCalendar.set(
				Calendar.DAY_OF_MONTH,
				Integer.parseInt(spittedVon[2].substring(0,
						spittedVon[2].indexOf("T"))));
		Date von = bisCalendar.getTime();

		Reservation res = new Reservation();
		res.setId(id);
		res.setFahrzeug(fahrzeug);
		res.setFiliale(filiale);
		res.setBis(bis);
		res.setVon(von);
		return res;
	}

	/**
	 * Adds all Reservation objects which are not already stored localy to the
	 * contentProvider. This is done via a id comparison
	 * 
	 * @param reservations
	 */
	private void addNewReservations2ContentProvider(
			List<Reservation> reservations) {
		// Get all ids of local existing reservations
		Cursor reservationIdCursor = queryIdsOfReservationsExistingLocaly(reservations);
		// get them as list of ids
		List<Integer> localyExistingReservationIds = extractLocalyExistingReservationIds(reservationIdCursor);
		// remove all reservations that locally already exists
		deleteLocalyExistingReservations(reservations,
				localyExistingReservationIds);
		// Put the rest (new ones) to the content provider
		addRest2ContentProvider(reservations);
	}

	/**
	 * Query for all ids of reservations stored behind the content provider
	 * 
	 * @param reservations
	 *            Reservations from server
	 * @return
	 */
	private Cursor queryIdsOfReservationsExistingLocaly(
			List<Reservation> reservations) {
		// Collecting all ids of the remote loaded reservations
		String[] ids = new String[reservations.size()];
		for (int i = 0; i < reservations.size(); i++) {
			Reservation reservation = reservations.get(i);
			ids[i] = String.valueOf(reservation.getId());
		}
		// BUILDING where clause "WHERE _id in (..)
		String selection = "_id IN (";
		for (int i = 0; i < ids.length; i++) {
			String string = ids[i];
			selection = selection.concat(string);
			if (i < ids.length - 1)
				selection = selection.concat(" ,");
		}
		selection = selection.concat(")");
		// Query
		Cursor reservationIdCursor = context.getContentResolver().query(
				ReservationContentProvider.CONTENT_URI,
				new String[] { ReservationContentProvider.ID_COLUMN_NAME },
				selection, null, null);
		return reservationIdCursor;
	}

	/**
	 * Reads the id column of all reservation entries in cursor and adds them to
	 * the list returned.
	 * 
	 * @param reservationIdCursor
	 * @return
	 */
	private List<Integer> extractLocalyExistingReservationIds(
			Cursor reservationIdCursor) {
		List<Integer> localyExistingReservations = new ArrayList<Integer>(
				reservationIdCursor.getCount());
		while (reservationIdCursor.moveToNext()) {
			int id = reservationIdCursor.getInt(0);
			localyExistingReservations.add(id);
		}
		return localyExistingReservations;
	}

	/**
	 * Iterate through reservations retreived from server and remove any which
	 * is stored locally. (If the id of the reservation matches to any id of the
	 * local Reservations)
	 * 
	 * @param reservations
	 * @param localyExistingReservationIds
	 */
	private void deleteLocalyExistingReservations(
			List<Reservation> reservations,
			List<Integer> localyExistingReservationIds) {
		for (Iterator<Reservation> iterator = reservations.iterator(); iterator
				.hasNext();) {
			Reservation reservation = iterator.next();
			if (localyExistingReservationIds.contains(reservation.getId()))
				iterator.remove();
		}
	}

	private void addRest2ContentProvider(List<Reservation> reservations) {
		for (Reservation reservation2Add : reservations) {
			ContentValues values = new ContentValues();
			values.put("_id", reservation2Add.getId());
			values.put("fahrzeug", reservation2Add.getFahrzeug());
			values.put("filiale", reservation2Add.getFiliale());
			values.put("bis", reservation2Add.getBis().getTime());
			values.put("von", reservation2Add.getVon().getTime());
			context.getContentResolver().insert(
					ReservationContentProvider.CONTENT_URI, values);
		}
	}

}

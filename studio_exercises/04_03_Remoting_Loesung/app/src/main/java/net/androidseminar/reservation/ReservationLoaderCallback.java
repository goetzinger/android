package net.androidseminar.reservation;

import android.app.ListFragment;
import android.app.LoaderManager.LoaderCallbacks;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.widget.CursorAdapter;

public class ReservationLoaderCallback implements LoaderCallbacks<Cursor> {

	private final ListFragment context;

	public ReservationLoaderCallback(ListFragment c) {
		this.context = c;
	}

	/**
	 * Initialize CursorLoader htat loads the Reservations from DB. Android OS
	 * will retreive the Data from the ContentProvider specified by the URI
	 */
	public Loader<Cursor> onCreateLoader(int id, Bundle args) {
		return new CursorLoader(context.getActivity(),
				ReservationContentProvider.CONTENT_URI, null,
				args.getString("selection"),
				args.getStringArray("selectionArgs"), null);
	}

	/**
	 * Called when the loader is finished. Setting Data to the adapter of the
	 * fragment.
	 */
	public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
		// Set the cursor to the list view, that makes the data visible to the
		// UI
		((CursorAdapter) context.getListAdapter()).swapCursor(data);
		// Activity should close the cursor in its destroy phase
		context.getActivity().startManagingCursor(data);
		// if Fragment is visible. Show list with data from cursor
		if (context.isResumed()) {
			context.setListShown(true);
		} else
			context.setListShownNoAnimation(true);

	}

	public void onLoaderReset(Loader<Cursor> loader) {
		((CursorAdapter) context.getListAdapter()).swapCursor(null);
	}

}

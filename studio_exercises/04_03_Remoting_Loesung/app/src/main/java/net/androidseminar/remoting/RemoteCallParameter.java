package net.androidseminar.remoting;

import java.io.Serializable;
import java.net.URI;

import android.os.AsyncTask;

/**
 * Wrapper class that stores URI and callback Information. As RemoteCall extends
 * {@link AsyncTask} it has to pass parameters of the same type to
 * {@link RemoteCall#doInBackground(RemoteCallParameter...)}. URI, and Callback
 * as parameters are not allowed, as they are not of the same type. <br/>
 * Simple solution: Introducing this wrapper class that holds both informations
 * and pass it to the {@link RemoteCall#doInBackground(RemoteCallParameter...)}
 * is a "workaround".
 * 
 * @author goetzingert
 * 
 */
@SuppressWarnings("serial")
public class RemoteCallParameter<T> implements Serializable {

	public URI uri;
	public RemoteCallback<T> callback;

}

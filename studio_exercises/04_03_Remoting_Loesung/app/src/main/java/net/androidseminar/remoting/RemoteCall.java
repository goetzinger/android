package net.androidseminar.remoting;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URI;
import android.os.AsyncTask;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RemoteCall extends
        AsyncTask<RemoteCallParameter<String>, String, String> {

    @Override
    protected String doInBackground(RemoteCallParameter<String>... params) {
        OkHttpClient client = new OkHttpClient();
        URI url = params[0].uri;

        Request request = null;
        try {
            request = new Request.Builder()
                    .url(url.toURL())
                    .header("Content-Type", "application/json")
                    .build();
            Response response = client.newCall(request).execute();
            String result =  response.body().string();
            // Call the handler that handle the result
            params[0].callback.handleResult(result);
            return result;

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private static String convertStreamToString(InputStream is) {
        /*
         * To convert the InputStream to String we use the
         * BufferedReader.readLine() method. We iterate until the BufferedReader
         * return null which means there's no more data to read. Each line will
         * appended to a StringBuilder and returned as String.
         */
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

}

package net.androidseminar.reservation;

import java.util.Date;

import net.androidseminar.remoting.R;
import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

class ReservationListAdapter extends CursorAdapter {

	private final Context context;
	private final java.text.DateFormat dateFormat = java.text.DateFormat
			.getDateInstance(java.text.DateFormat.SHORT);

	ReservationListAdapter(Context context, Cursor cursor) {
		super(context, cursor);
		this.context = context;
	}

	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		View view;

		LayoutInflater inflater = LayoutInflater.from(context);
		view = inflater.inflate(R.layout.reservationlistentry, null, false);

		TextView fahrzeugText = ((TextView) view
				.findViewById(R.id.fahrzeugText));
		fahrzeugText
				.setText(cursor.getString(cursor.getColumnIndex("fahrzeug")));
		TextView filialeText = ((TextView) view.findViewById(R.id.filialeText));
		filialeText.setText(cursor.getString(cursor.getColumnIndex("filiale")));
		TextView vonText = ((TextView) view.findViewById(R.id.vonText));
		long vonDateAsMilliseconds = cursor.getLong(cursor
				.getColumnIndex("von"));
		vonText.setText(dateFormat.format(new Date(vonDateAsMilliseconds)));
		TextView bisText = ((TextView) view.findViewById(R.id.bisText));
		long bisDateAsMilliseconds = cursor.getLong(cursor
				.getColumnIndex("bis"));
		bisText.setText(dateFormat.format(new Date(bisDateAsMilliseconds)));

		return view;
	}

	@Override
	public void bindView(View view, Context context, Cursor cursor) {
		TextView fahrzeugText = ((TextView) view
				.findViewById(R.id.fahrzeugText));
		fahrzeugText
				.setText(cursor.getString(cursor.getColumnIndex("fahrzeug")));
		TextView filialeText = ((TextView) view.findViewById(R.id.filialeText));
		filialeText.setText(cursor.getString(cursor.getColumnIndex("filiale")));
		TextView vonText = ((TextView) view.findViewById(R.id.vonText));
		long vonDateAsMilliseconds = cursor.getLong(cursor
				.getColumnIndex("von"));
		vonText.setText(dateFormat.format(new Date(vonDateAsMilliseconds)));
		TextView bisText = ((TextView) view.findViewById(R.id.bisText));
		long bisDateAsMilliseconds = cursor.getLong(cursor
				.getColumnIndex("bis"));
		bisText.setText(dateFormat.format(new Date(bisDateAsMilliseconds)));

	}

}

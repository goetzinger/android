package net.androidseminar.reservation;

import net.androidseminar.remoting.R;
import android.app.ListFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class AllReservationsFragment extends ListFragment {

	private ReservationListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// initialize Loader to load local data from db.
		// ReservationLoaderCallback will be called asynchron
		this.getLoaderManager().initLoader(0, new Bundle(),
				new ReservationLoaderCallback(this));
		// Adapter with null cursor, because data will be retreived by loader
		adapter = new ReservationListAdapter(getActivity(), null);
		// Start Service to load Data from Remote Server
		getActivity().startService(
				new Intent(getActivity(), LoadAllReservationsService.class));
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		// setting adapeter
		setListAdapter(adapter);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		// hide list show Progress
		setListShown(false);
		// Set String when no entry is present
		setEmptyText(getResources().getString(R.string.noContentInList));
	}

}

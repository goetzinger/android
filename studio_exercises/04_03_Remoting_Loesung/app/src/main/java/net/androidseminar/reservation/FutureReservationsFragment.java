package net.androidseminar.reservation;

import java.util.Calendar;

import net.androidseminar.remoting.R;
import android.app.ListFragment;
import android.os.Bundle;
import android.view.View;

public class FutureReservationsFragment extends ListFragment {

	private ReservationListAdapter adapter;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Generating query where clause
		// Only Reservations with von > currentTime should be retreived from
		// content provider
		long nowInMillis = Calendar.getInstance().getTime().getTime();
		Bundle args = new Bundle();
		args.putString("selection", "von > ?");
		args.putStringArray("selectionArgs", new String[] { nowInMillis + "" });
		this.getLoaderManager().initLoader(0, args,
				new ReservationLoaderCallback(this));
		adapter = new ReservationListAdapter(getActivity(), null);
		// Because AllReservationsFragment loads all data from Remote, it is not
		// necessary to load it here too.

	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);
		setListAdapter(adapter);

	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setListShown(false);
		setEmptyText(getResources().getString(R.string.noContentInList));
	}

}
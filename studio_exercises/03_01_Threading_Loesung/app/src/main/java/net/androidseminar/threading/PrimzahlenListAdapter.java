package net.androidseminar.threading;

import java.util.LinkedList;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class PrimzahlenListAdapter extends BaseAdapter {

	private final List<Integer> primzahlen = new LinkedList<Integer>();
	private final Context context;

	public PrimzahlenListAdapter(Context context) {
		this.context = context;
	}

	public int getCount() {
		return primzahlen.size();
	}

	public Object getItem(int position) {
		return primzahlen.get(position);
	}

	public long getItemId(int position) {
		return primzahlen.get(position);
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			LayoutInflater layoutInflater = LayoutInflater.from(context);
			convertView = layoutInflater.inflate(R.layout.primzahleneintrag,
					null, false);
		}
		TextView textView = (TextView) convertView
				.findViewById(R.id.primzahlText);
		textView.setText("" + primzahlen.get(position));
		return convertView;
	}

	public void add(Integer i) {
		primzahlen.add(0, i);
		super.notifyDataSetChanged();
	}

	public void clear() {
		this.primzahlen.clear();
		super.notifyDataSetChanged();
	}

}

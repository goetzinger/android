package net.androidseminar.threading;

import android.app.ListActivity;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.Toast;

public class ThreadingActivity extends ListActivity {
	private PrimzahlenListAdapter adapter;
	private AsyncTask<Integer, Integer, Void> asyncTask;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		adapter = new PrimzahlenListAdapter(this.getApplicationContext());
		super.setListAdapter(adapter);
		getActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public boolean onCreateOptionsMenu(android.view.Menu menu) {
		MenuInflater menuInflater = getMenuInflater();
		menuInflater.inflate(R.menu.threadingmenu, menu);
		return true;
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		super.onOptionsItemSelected(item);
		switch (item.getItemId()) {
		case R.id.start_menu:
			start();
			return true;
		case R.id.stop_menu:
			stop();
			return true;
		default:
			return false;
		}

	}

	private void stop() {
		if (asyncTask != null && asyncTask.getStatus().equals(Status.RUNNING)) {
			asyncTask.cancel(true);
			asyncTask = null;
		} else
			Toast.makeText(getApplicationContext(), "Task not running!", 2000);
	}

	private void start() {
		if (asyncTask == null || asyncTask.getStatus().equals(Status.FINISHED))
			asyncTask = new PrimzahlenSucheAsyncTask(adapter).execute(10000);
		else
			Toast.makeText(getApplicationContext(), "Task already running!",
					2000);

	}
}
package net.androidseminar.threading;

import android.os.AsyncTask;

public class PrimzahlenSucheAsyncTask extends AsyncTask<Integer, Integer, Void> {

	private final PrimzahlenListAdapter adapter;

	public PrimzahlenSucheAsyncTask(PrimzahlenListAdapter adapter) {
		this.adapter = adapter;
		this.adapter.clear();
	}

	@Override
	protected Void doInBackground(Integer... params) {
		for (int i = 3; i <= params[0]; i++) {
			boolean isPrim = isPrim(i);
			if (isPrim)
				this.publishProgress(i);
		}
		return null;
	}

	private boolean isPrim(int i) {
		for (int j = 2; j < i; j++) {
			if (i % j == 0)
				return false;
		}
		return true;
	}

	@Override
	protected void onProgressUpdate(Integer... values) {
		super.onProgressUpdate(values);
		adapter.add(values[0]);
	}

}
